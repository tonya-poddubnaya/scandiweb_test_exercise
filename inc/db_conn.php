<?php

$host = 'localhost';
$user = 'root';
$passwd = '';
$schema = 'testexercise';

$conn = mysqli_connect($host, $user, $passwd, $schema);

if (!$conn)
{
   echo 'Connection failed<br>';
   die();
}

return $conn;