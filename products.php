<?php

    include('header.php');
    include('inc/db_conn.php');

    $sql = "SELECT * FROM products";
    $result = $conn->query($sql);
?>
<form action='inc/mass_delete.php' method='post'>
    <div class='navbar'>
        <h2>Product List</h2>
        <div class='inline right navbar_form'>
            <select>
                <option value='delete'>Mass Delete Action</option>
            </select>
            <button name='delete' class='btn submit' type='submit'>Delete</button>
        </div>
    </div>
    <hr>
    <div class='row'>

<?php
    if ($result->num_rows > 0) 
    {
        while($row = $result->fetch_assoc()) 
        {
            if($row['category'] == 1){$attribute = 'Size: '.$row["attribute_value"].' MB';};
            if($row['category'] == 2){$attribute = 'Weight: '.$row["attribute_value"].' KG';};
            if($row['category'] == 3){$attribute = 'Dimension: '.$row["attribute_value"].'';};
            echo "
            <div class='column'>
                
                <div class='product_card'>
                <input type='checkbox' name='checkbox[]' value=".$row['id']."/>
                    <h4><b>".$row['product_name']."</b></h4>
                    <p>".$row['SKU']."</p>
                    <p>".$row['price']." $</p>
                    <p>".$attribute."</p>
                </div>
            </div>
            ";
        }
    } 
    else 
    {
        echo "No results";
    }
?>

    </div>
    <a href='/new' class='add_new_button'>Add</a>
</form> 

<?php
$conn->close();