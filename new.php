
<?php
include('header.php');
?>
<a href='/products'><h4>Product List</h4></a>
<form class='new_form' action="inc/create_product.php" method="post">
    <div class='form_container'>
        <h1>Add New Product</h1>
        <label for='sku'>SKU</label>
        <input type='text' name='sku' required/>

        <label for='name'>Name</label>
        <input type='text' name='product_name' required/>

        <label for='price'>Price</label>
        <input type='number' name='price' min='0' step="0.01" required/>

        <label for='category'>Type Switcher</label>
        <select name='category' id='category' required>
            <option value=''>Type Switcher</option>
            <option value='1'>DVD-Disc</option>
            <option value='2'>Book</option>
            <option value='3'>Furniture</option>
        </select>

        <div data-choice="1">
            <label for='size'>Size</label>
            <input name='size' id='size' type="number" placeholder="Size in MB" step="0.01" min='0' >
        </div>
        <div data-choice="2">
            <label for='weight'>Weight</label>
            <input name='weight' id='weight' type="number" placeholder="Weight in KG" step="0.01" min='0' >
        </div>
        <div data-choice="3">
            <label for='H'>Height</label>
            <input name='H' id='H' type="number" placeholder="Height" step="0.01" min='1' >
            <label for='W'>Width</label>
            <input name='W' id='W' type="number" placeholder="Width" step="0.01" min='1' >
            <label for='L'>Lenght</label>
            <input name='L' id='L' type="number" placeholder="Lenght" step="0.01" min='1' >
        </div>

        <button class='btn submit' type="submit">Add Product</button>
    </div>
</form>

<script>
    $(function() {
        $("select").on("change", function() {
            if($(this).val() === "") {
                $("[data-choice]").hide();
                $("div[data-choice] input").removeAttr('required');
            } else {
                $("div[data-choice] input").removeAttr('required');
                $("div[data-choice='" + $(this).val() + "'] input").attr('required', true);
                $("div[data-choice='" + $(this).val() + "']").show().siblings("[data-choice]").hide();
            }
        });
    });
</script>